import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './src/screens/HomeScreen';
import GameScreen from './src/screens/GameScreen';
import { WordContextProvider } from './src/context/wordContext';
import EndScreen from './src/screens/EndScreen';


const App = () => {
  const Stack = createNativeStackNavigator();
  return (
    <WordContextProvider>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Game" component={GameScreen} />
          <Stack.Screen name="EndScreen" component={EndScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
      </NavigationContainer>
    </WordContextProvider>
  );
};


export default App;
