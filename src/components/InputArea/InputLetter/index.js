import React, { useContext, useEffect } from 'react';
import styled from 'styled-components';
import WordContext from '../../../context/wordContext';

const InputLetter = (props) => {
    const { changeAnswer } = useContext(WordContext)

    return (
        <LetterContainer onPress={() => changeAnswer(props.letter)}>
            <TextLetter>
                {props.letter}
            </TextLetter>
        </LetterContainer>
    );
};

const LetterContainer = styled.TouchableOpacity`
    border: 1px solid #fff;
    background-color: transparent;
    justify-content: center;
    align-items: center;
    border-radius: 8px;
    height: 50px;
    width: 50px;
    color: #fff;
    margin: 2px;
    
`
const TextLetter = styled.Text`
    color: #fff;
    font-weight: bold;
`
export default InputLetter;