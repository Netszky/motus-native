import React, { useContext, useState } from 'react';
import styled from 'styled-components';
import WordContext from '../../context/wordContext';
import { alphabet } from '../../utils/utils';
import InputLetter from './InputLetter';


const InputArea = (props) => {
    const { deleteCharacter, onValidate } = useContext(WordContext);

    return (
        <Input>
            {alphabet.map((letter) => {
                return (
                    <InputLetter key={letter} letter={letter} />
                )
            })}
            <LetterContainer onPress={() => deleteCharacter()}>
                <TextLetter >
                    ⌫
                </TextLetter>
            </LetterContainer>
            <LetterContainer onPress={() => { onValidate() }}>
                <TextLetter>
                    ↲
                </TextLetter>
            </LetterContainer>
        </Input >
    );
};

const Input = styled.View`
    height: 250px;
    width: 80%;
    justify-content: center;
    align-items: center;
    flex: 1;
    flex-direction: row;
    flex-wrap: wrap;
    
`
const LetterContainer = styled.TouchableOpacity`
    border: 1px solid #fff;
    background-color: transparent;
    justify-content: center;
    align-items: center;
    border-radius: 8px;
    height: 50px;
    width: 50px;
    color: #fff;
    margin: 2px;

`
const TextLetter = styled.Text`
    color: #fff;
    font-weight: bold;
`
export default InputArea;