import React, { useContext, useEffect, useState } from 'react';
import { TextInput } from 'react-native';
import styled from 'styled-components';
import WordContext from '../../context/wordContext';
import Square from '../Square';

const Grid = (props) => {
    const { answer, guessedWords } = useContext(WordContext);
    const GenerateSquare = ({ guessedWord }) => {
        const tab = [];
        for (let index = 0; index < props.test.length; index++) {
            tab.push(<Square letter={guessedWord.charAt(index)} word={props.test} key={index} index={index} verify={false} />);
        }
        return tab;
    }
    const GenerateSquare2 = (word) => {
        const tab = [];
        for (let index = 0; index < props.test.length; index++) {
            tab.push(<Square letter={word.charAt(index)} word={props.test} key={index} index={index} verify={true} />);
        }
        return tab;
    }
    return (
        <Container>
            <TextInput style={{ display: 'none' }} value={answer.input}>

            </TextInput>
            <GridContainer>
                <Row>
                    {
                        guessedWords[0] ?

                            GenerateSquare2(guessedWords[0])

                            :

                            GenerateSquare({ guessedWord: answer.try === 0 ? answer.input : "" })
                    }
                </Row>
                <Row>
                    {
                        guessedWords[1] ?

                            GenerateSquare2(guessedWords[1])

                            :
                            GenerateSquare({ guessedWord: answer.try === 1 ? answer.input : "" })}
                </Row>
                <Row>
                    {guessedWords[2] ?

                        GenerateSquare2(guessedWords[2])

                        :
                        GenerateSquare({ guessedWord: answer.try === 2 ? answer.input : "" })}
                </Row>
                <Row>
                    {guessedWords[3] ?

                        GenerateSquare2(guessedWords[3])

                        :
                        GenerateSquare({ guessedWord: answer.try === 3 ? answer.input : "" })}
                </Row>
                <Row>
                    {guessedWords[4] ?

                        GenerateSquare2(guessedWords[4])

                        :
                        GenerateSquare({ guessedWord: answer.try === 4 ? answer.input : "" })}
                </Row>
                <Row>
                    {
                        guessedWords[5] ?

                            GenerateSquare2(guessedWords[5])

                            :
                            GenerateSquare({ guessedWord: answer.try === 5 ? answer.input : "" })}
                </Row>
            </GridContainer>
        </Container>
    );
};
const Container = styled.View`
    height: 60%;
    width: 100%;
    background-color: rgb(43,43,43);
    align-items: center;
`
const GridContainer = styled.View`
    height: 360px;
    width: 300px;
    background-color: rgb(0,119,199);
    margin-top: 20px;
    border: 1px solid #fff;
`
const Row = styled.View`
    flex-direction: row;
    width: 100%;
    height: 60px;
    border: 1px solid #fff;
`
export default Grid;