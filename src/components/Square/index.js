import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const Square = (props) => {
    const [isIn, setIsIn] = useState(0);
    useEffect(() => {
        if (props.verify) {
            if (props.word.includes(props.letter)) {
                if (props.word.charAt(props.index) === props.letter) {
                    setIsIn(2);
                } else if (props.word.includes(props.letter)) {
                    setIsIn(1)
                }
            } else {
                setIsIn(0);
            }
        } else {
            null
        }
    }, [props.verify])
    return (
        <>

            {props.index === 0 ?
                <Squar2>
                    <Letter>{props.word.toUpperCase().charAt(0)}</Letter>
                </Squar2> :
                <>
                    {
                        props.letter ?
                            isIn === 0 ?
                                <Squar1>
                                    <Letter>{props.letter}</Letter>
                                </Squar1>
                                : isIn === 1 ?
                                    <Squar3>
                                        <Letter>{props.letter}</Letter>
                                    </Squar3>
                                    :
                                    <Squar2>
                                        <Letter>{props.letter}</Letter>
                                    </Squar2>
                            :
                            <Squar>
                                <Letter></Letter>
                            </Squar>
                    }
                </>
            }
        </>
    );

};

const Squar = styled.View`
        flex: 1;
        height: 60px;
        background-color: rgb(0,119,199);
        border: 1px solid #fff;
        align-items: center;
        justify-content: center;
    `
const Squar1 = styled.View`
flex: 1;
height: 60px;
background-color: rgb(0,119,199);
border: 1px solid #fff;
align-items: center;
justify-content: center;
`
const Squar2 = styled.View`
        flex: 1;
        height: 60px;
        background-color: red;
        border: 1px solid #fff;
        align-items: center;
        justify-content: center;
    `
const Squar3 = styled.View`
    flex: 1;
    height: 60px;
    border-radius: 100px;
    background-color: #E0D22C;
    border: 1px solid #fff;
    align-items: center;
    justify-content: center;
`
const Letter = styled.Text`
        font-size: 35px;
        color: #fff;
        font-weight: bold;
    `

export default Square;