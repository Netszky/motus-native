import React, { useEffect, useState } from 'react';
import { Dimensions, Image } from 'react-native';
import Champions from '../../../champion.json';
import styled from 'styled-components';
import logo from '../../assets/lol.png';
import SoundPlayer from 'react-native-sound-player'
import { useNavigation } from '@react-navigation/native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const HomeScreen = () => {
  const [championsList, setChampionsList] = useState([]);
  const test = Champions;
  const list = [];
  const navigation = useNavigation();
  useEffect(() => {
    SoundPlayer.playSoundFile('motus', 'mp3');
    Object.keys(test.data).forEach(key => {
      list.push(test.data[key].id)
    })
    setChampionsList(list);
  }, [])
  return (

    <Container>
      <LogoContainer>
        <Image style={{ width: 300 }} resizeMode='contain' source={logo}></Image>
      </LogoContainer>
      <Title>Motus LOL</Title>
      <MainButton onPress={() => {
        navigation.navigate("Game", {
          list: championsList
        }), SoundPlayer.playSoundFile('summoners', "mp3")
      }}>
        <ButtonText>Jouer !</ButtonText>
      </MainButton>
    </Container>
  );
};
const Container = styled.View`
  height: ${windowHeight}px;
  width: ${windowWidth}px;
  background-color: #fff;
  align-items: center;
`
const Title = styled.Text`
  color: #000;
  font-size: 22px;
  font-weight: bold;
`
const MainButton = styled.TouchableOpacity`
  margin-top: 200px;
  border: 2px solid #000;
  padding: 20px;
  border-radius: 8px;
`
const ButtonText = styled.Text`
  color: #000;
  font-size: 20px;
`
const LogoContainer = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
`
export default HomeScreen;