import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useContext } from 'react';
import styled from 'styled-components';
import WordContext from '../../context/wordContext';

const EndScreen = ({ route }) => {
    const { type, word } = route.params;
    const { resetAll } = useContext(WordContext)
    const navigation = useNavigation();
    useEffect(() => {
        setTimeout(() => {
            resetAll();
            navigation.navigate("Home");

        }, 4000);
    }, [])

    return (
        <MainView>
            <TextStatus>{type === "win" ? 'Victoire' : 'Defaite'}</TextStatus>
            <TextStatus>Le mot etait : {word} </TextStatus>
        </MainView >
    );
};


const MainView = styled.View`
    height: 100%;
    width: 100%;
    background-color: rgba(43,43,43,1);
    align-items: center;
    justify-content: center;

    `
const TextStatus = styled.Text`
    color: #fff;
    font-size: 30px;
`
export default EndScreen;