import { useNavigation } from '@react-navigation/native';
import React, { useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import Grid from '../../components/Grid';
import InputArea from '../../components/InputArea';
import WordContext from '../../context/wordContext';

const GameScreen = ({ route }) => {
    const navigation = useNavigation();
    const { changeWord, changeAnswer, changeTry, resetAll, isEnd, answer } = useContext(WordContext);
    const { list } = route.params;
    const [word2, setWord2] = useState('');

    useEffect(() => {
        if (isEnd === 1) {
            navigation.navigate("EndScreen", { type: "win", word: word2 })
        }
        if (isEnd === 2) {
            navigation.navigate("EndScreen", { type: "lose", word: word2 })
        }
    }, [isEnd])
    useEffect(() => {
        setWord2(list[Math.floor(Math.random() * list.length + 1)].toUpperCase())
        changeWord(word2);
        changeTry(0);
    }, [])
    useEffect(() => {
        resetAll();
        if (word2.length > 0) {
            changeAnswer(word2[0]);
            changeWord(word2);
        }
        else {
            return
        }
    }, [word2])
    return (
        <Container>
            {word2.length > 0 &&
                <Grid test={word2} >

                </Grid>
            }
            {
                answer.input.length > 0 &&
                <InputArea />
            }
        </Container>
    );
};


const Container = styled.View`
    height: 100%;
    width: 100%;
    background-color: rgb(43,43,43);
    align-items: center;
`
const RestartButton = styled.TouchableOpacity`
    background-color: rgb(0,119,199);
    padding: 20px;
    border-radius: 8px;
`
const RestartText = styled.Text`
    color: #fff;

`
const Modal = styled.View`
    position: absolute;
    height: 100%;
    width: 100%;
    background-color: #fff;

`
export default GameScreen;