import React, { createContext, useEffect, useState } from 'react';

const WordContext = createContext({
    guessedWords: [],
    isEnd: 0,
    tryNb: 0,
    answer: {},
    word: "",
    changeLetter: () => { },
    changeAnswer: () => { },
    changeWord: () => { },
    deleteCharacter: () => { },
    onValidate: () => { },
    changeTry: () => { },
    resetAll: () => { },
    getGuessedWord: () => { }
})

export const WordContextProvider = ({ children }) => {
    const [guessedWords, setGuessedWord] = useState([]);
    const [tryNb, setTryNb] = useState(0)
    const [word, setWord] = useState("");
    const [answer, setAnswer] = useState({ input: word.charAt(0), try: 0 });
    const [isEnd, setIsEnd] = useState(0);
    const onValidate = () => {
        console.log(tryNb)
        if (answer.input.length < word.length) {
            return
        } else {
            if (answer.input === word) {
                setIsEnd(1);
                setGuessedWord([...guessedWords, answer.input])
            } else {
                if (tryNb === 5) {
                    setIsEnd(2)
                } else {
                    setTryNb(tryNb + 1)
                    setGuessedWord([...guessedWords, answer.input])
                    setAnswer({ input: word.charAt(0) })
                }
            }
        }
    }
    const changeAnswer = (newWord) => {
        if (answer.input.length < word.length) {
            setAnswer({ input: answer.input + newWord, try: tryNb });
        } else {
            return
        }
    };
    const getGuessedWord = () => {
        return guessedWords
    }
    const deleteCharacter = () => {
        if (answer.input.length > 1) {
            const test = answer.input.slice(0, -1)
            setAnswer({ input: test, try: tryNb });
        } else {
            return
        }
    }
    const changeWord = (randomWord) => {
        setWord(randomWord);
    }
    const resetAll = () => {
        setGuessedWord([])
        setIsEnd(0);
        setAnswer({ input: word.charAt(0) });
    }
    const changeTry = (nb) => {
        setTryNb(nb)
    }
    useEffect(() => {
        setAnswer({ input: word.charAt(0) })
    }, [word])
    const context = { isEnd, getGuessedWord, answer, changeAnswer, changeWord, changeTry, deleteCharacter, onValidate, guessedWords, resetAll }

    return (
        <WordContext.Provider value={context}>
            {children}
        </WordContext.Provider>
    )
}

export default WordContext;