# Motus-Native
Mini Projet de M1 Dev Web par Chigot Julien.

## Projet

Remake du jeu Motus basé sur les champions de League of Legend !

- L'application tire le nom d'un champion aléatoire
- Vous connaissez la longueur du mot et la première lettre a vous de trouver le reste.
- Si une lettre est à la bonne place elle sera marquée en rouge
- Si une lettre existe mais n'est pas bien placée elle sera marquée en jaune (Voir section Bug / Amelioration)
- Si la lettre n'existe pas rien ne se passe.

## Bug / Améliorations

- Ajouter une api Dictionnaire pour jouer au jeu original.
- Ameliorer l'algorithme de vérification des mots : Si une lettre existe plusieurs fois ou a déja été trouvé ne plus la vérifier...


## Lancement du projet

- Clone Projet
- npm install
- npx react-native start
- npx react-native run-android

## Devices
Ce projet à été développé sur Android et non testé sur iOS.
